﻿using System;
using Assertions.Assertions;

namespace Assertions.Extensions
{
    public static class AssertionExtension
    {
        public static NumberAssertions Expect(this int currentValue)
        {
            return new NumberAssertions(currentValue);
        }

        public static ActionAssertions Expect(this Action assertion)
        {
            return new ActionAssertions(assertion);
        }

        public static ObjectAssertions<T> Expect<T>(this T assertion)
        {
            return new ObjectAssertions<T>(assertion);
        }
    }
}
