﻿using System;

namespace Assertions.Exceptions
{
    public class ExpectationFailedException : Exception
    {
        public ExpectationFailedException(string message)
        {
            Message = message;
        }

        public override string Message { get; }
    }
}
