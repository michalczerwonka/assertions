﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Assertions.Assertions
{
    public class ObjectAssertions<T>
    {
        private readonly T _value;

        public ObjectAssertions(T value)
        {
            _value = value;
        }

        public PropertiesAssertions<T> Properties()
        {
            return new PropertiesAssertions<T>(_value);
        }

        public PropertiesAssertions<T> PropertiesWithout<TProperty>(Expression<Func<T, TProperty>> propertyLambda)
        {
            var member = propertyLambda.Body as MemberExpression;
            var propInfo = member.Member as PropertyInfo;
            return new PropertiesAssertions<T>(_value, propInfo.Name);
        }
    }
}
