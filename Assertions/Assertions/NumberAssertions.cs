﻿using Assertions.Exceptions;

namespace Assertions.Assertions
{
    public class NumberAssertions
    {
        private readonly int _value;
        private readonly bool _notAssertion;

        public NumberAssertions(int value, bool notAssertion = false)
        {
            _value = value;
            _notAssertion = notAssertion;
        }

        public NumberAssertions Not()
        {
            var assertion = new NumberAssertions(_value, true);
            return assertion;
        }

        public void Eq(int expectedValue)
        {
            if (Equals(_value, expectedValue) == !_notAssertion)
            {
                return;
            }

            throw new ExpectationFailedException($"Expected {expectedValue} to be equal {_value}");
        }

        public void IsGreater(int expectedValue)
        {
            if (_value > expectedValue == !_notAssertion)
            {
                return;
            }

            throw new ExpectationFailedException($"Expected {expectedValue} to be greater than {_value}");
        }
    }
}