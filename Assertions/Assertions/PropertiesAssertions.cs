﻿using Assertions.Exceptions;

namespace Assertions.Assertions
{
    public class PropertiesAssertions<T>
    {
        private readonly T _value;
        private readonly string _withoutPropertyName;

        public PropertiesAssertions(T value, string withoutPropertyName = null)
        {
            _value = value;
            _withoutPropertyName = withoutPropertyName;
        }

        public void Eq(object expected)
        {
            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                if (_withoutPropertyName == property.Name) continue;
                var propertyName = property.Name;

                var value1 = GetPropertyValue(_value, propertyName);
                var value2 = GetPropertyValue(expected, propertyName);
                if (!Equals(value1, value2))
                    throw new ExpectationFailedException($"Properties {propertyName} are not equals");
            }
        }

        public static object GetPropertyValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName)?.GetValue(src, null);
        }
    }
}