﻿using System;
using Assertions.Exceptions;

namespace Assertions.Assertions
{

    public class ActionAssertions
    {
        private readonly Action _value;

        public ActionAssertions(Action value)
        {
            _value = value;
        }

        public void RaiseError()
        {
            var exceptionRaised = false;
            try
            {
                _value.Invoke();
            }
            catch
            {
                exceptionRaised = true;
            }

            if (exceptionRaised)
            {
                return;
            }
            throw new ExpectationFailedException($"Expected Exception");
        }
    }
}
